
from PyPDF2 import PdfFileReader, PdfFileWriter

# req : pip install PYPDF2

file_path = 'some copy able pdf'
pdf = PdfFileReader(file_path)
for page_num in range(pdf.numPages):
    page_obj = pdf.getPage(page_num)
    text = page_obj.extractText().encode('utf-8')
    print(f'Page : {page_num}')
    if text:
        print("found something")
